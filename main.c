#include <pthread.h>
#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <unistd.h>

void *print_message(void *msg);

const int NTHREADS = 5;

int main(void){
    pthread_t threads[NTHREADS];
    pthread_attr_t attr;
    void *status;
    
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);

    long i = 0;
    for (i = 0; i < NTHREADS; ++i) {
        int create_code = pthread_create(&threads[i], &attr, print_message, (void*)i);
        
        if (create_code) {
            printf("ERROR!");
            return EXIT_FAILURE; 
       }
    }
    
    pthread_attr_destroy(&attr);
    
    for (i = 0; i < NTHREADS; ++i) {
        int join_code = pthread_join(threads[i], &status);
        int status_returned = (long) status;
        
        if (join_code || status_returned != i){
            return EXIT_FAILURE;
        }
    }
    
    printf("I AM MOTHER!\n");
    pthread_exit(NULL);
    return 0;
}

void *print_message(void *msg) {
    printf("%ld|%p\n", (long) msg, &msg);
    pthread_exit((void*) msg);
}